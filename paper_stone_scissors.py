import random

def who_win (p1, p2):
    """check and return who wins and who loses in two strings"""
    if p1.lower() == p2.lower():
        player1 = "Draw"
        player2 = "Draw"
    elif ((p1.lower() == "paper") and (p2.lower() == "stone")) or ((p1.lower() == "stone") and (p2.lower() == "scissors")) or ((p1.lower() == "scissors") and (p2.lower() == "paper")):
        player1 = "Win"
        player2 = "Lose" 
    else:
        player1 = "Lose"
        player2 = "Win"       
    return player1, player2



print("Welcom to The Game Paper, Stone, Scrissors\nChoose your game: Single or Multi")
single_multi = input()
pss = ["paper","stone","scissors"]

while True:
#Choose single or multiplayer
    while single_multi.lower() not in ["single","multi"]:
        single_multi = input("Single or Multi\n")
    if single_multi.lower() == "single":
        print("\nYou vs Computer")
        choose = input("What you choose ?\nPaper | Stone | Scissors    or exit\n")
        if choose.lower() == "exit":
            print("GoodBye")
            break
        else:
#Singleplayer
            while choose.lower() not in pss:
                choose = input("Pleas write correct: \nPaper | Stone | Scissors        or exit\n")
            
            computer_choose = random.choice(pss)             
            print(f"Computer choose {computer_choose}")
            you_win_or_lose, computer_win_or_lose = who_win(choose, computer_choose)
            print(f"You: {you_win_or_lose}, Computer: {computer_win_or_lose}\n")
#Multuplayer
    elif single_multi.lower() == "multi":
        print("Now it is a multiplayer game for 2 players\nCover the monitor with a blanket and Player One choose:\nPaper | Stone | Scissors       or exit\n")
        player_one = input()
        if player_one.lower() == "exit":
            print("GoodBye")
            break
        else:
            while player_one.lower() not in pss:
                player_one = input("Pleas write correct: \nPaper | Stone | Scissors      or exit\n")
                
        player_two = input("Don't look at what the previous player typed and choose:\nPaper | Stone | Scissors       or exit\n")
        if player_two.lower() == "exit":
            print("GoodBye")
            break
        else:
            while player_two.lower() not in pss:
                player_two = input("Pleas write correct: \nPaper | Stone | Scissors\n")

        player_one_win_or_lose, player_two_win_or_lose = who_win(player_one, player_two)
        print(f"Player 1: {player_one_win_or_lose}, Player 2: {player_two_win_or_lose}\n")

    elif single_multi.lower() == "exit":
        print("Goodby")
        break
